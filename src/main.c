#include <nrf.h>

#include <stdio.h>

#include "uart.h"
#include "radio.h"

#define LED_PIN 18

//Uncomment for tern!
//#define LED_PIN 10

int main()
{
    int i;
    int j=0;
    char string[100];
    RadioPacket rxPacket;

    // Enable reset when in debug mode
    //NRF_POWER->RESET = 1;


    NRF_CLOCK->TASKS_HFCLKSTART = 1;

    while(!NRF_CLOCK->EVENTS_HFCLKSTARTED);

    uartInit();


    // NRF_GPIO->PIN_CNF[LED_PIN] |= GPIO_PIN_CNF_DIR_Output | (GPIO_PIN_CNF_DRIVE_S0H1<<GPIO_PIN_CNF_DRIVE_Pos);
    // NRF_GPIO->PIN_CNF[19] |= GPIO_PIN_CNF_DIR_Output | (GPIO_PIN_CNF_DRIVE_S0H1<<GPIO_PIN_CNF_DRIVE_Pos);

    nrfSetDatarate(RADIO_RATE_2M);
    nrfSetChannel(66);

    nrfInit();


    while(1)
    {
        if (nrfIsPacketReceived())
        {
          RadioPacket* packet = nrfGetRxPacket();
          sprintf(string, "Addr %d, Pid %02x, Noack %1d, %3ddBm, ", packet->address, packet->raw.pid, packet->raw.noack, -1*packet->rssi);
          uartPuts(string);
          for (i=0; i<packet->raw.size; i++) {
            sprintf(string, "%02x ", packet->raw.data[i]);
            uartPuts(string);
          }
          uartPuts("\r\n");
          nrfReleaseRxPacket(packet);
        }
    }

    return 0;
}

