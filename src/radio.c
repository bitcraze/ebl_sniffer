/*
 *    ||          ____  _ __
 * +------+      / __ )(_) /_______________ _____  ___
 * | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Crazyflie control firmware
 *
 * Copyright (C) 2011-2012 Bitcraze AB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, in version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * nrf24l01.c: nRF24L01(-p) PRX mode low level driver
 */

#include "radio.h"

#include <stdbool.h>
#include <string.h>

#include "nrf51.h"
#include "nrf51_bitfields.h"

// S1 is used for compatibility with NRF24L0+. These three bits are used
// to store the PID and NO_ACK.
#define PACKET0_S1_SIZE                  (3UL)
// S0 is not used
#define PACKET0_S0_SIZE                  (0UL)
// The size of the packet length field is 6 bits
#define PACKET0_PAYLOAD_SIZE             (6UL)
// The size of the base address field is 4 bytes
#define PACKET1_BASE_ADDRESS_LENGTH      (4UL)
// Don't use any extra added length besides the length field when sending
#define PACKET1_STATIC_LENGTH            (0UL)
// Max payload allowed in a packet
#define PACKET1_PAYLOAD_SIZE             (32UL)


#define PKQ_LEN 16       // Big enough buffer to catch retry!

/* Radio transmissions states */
typedef enum {doTx, doRx} RadioStates;

/* Private variables */
static bool isInit;
static RadioPacket rxPacket[PKQ_LEN];
static RadioPacket txPacket;
static bool packetSent = true;
static bool newTxPacket = false;
static bool rerunRx;
static RadioPacket ackPacket;
RadioStates rs;

int pkq_head = 0;
int pkq_tail = 0;

/* RX is double buffered, TX is single
 * Buffers are busy when owned by the main loop! (rx starts IDLE and tx starts busy)
 */
static struct radioBuffer rxBuffers[2];
static struct radioBuffer txBuffer;
static int currentRxBuffer = 0;

/* The transmision process will pause (stop) if no-on is fetching the data
 * (It is very possible that this may be a terrible idea!)
 */
static bool running = false;


static bool packetReceived=false;


static int pkq_inc(int ptr)
{
  ptr = ptr+1;
  if (ptr>=PKQ_LEN) ptr = 0;

  return ptr;
}


/**
 * Enable radio RX.
 */
static __inline void enableRx(void)
{
	rs = doRx;
	NRF_RADIO->PACKETPTR = (uint32_t) &rxPacket[pkq_head].raw;
  NRF_RADIO->EVENTS_READY = 0U;
  NRF_RADIO->TASKS_RXEN = 1U;
}

/**
 * The radio needs to be disabled when switching between
 * RX/TX. The pointer to the data payload also needs to be
 * re-written.
 */
//static __inline void disableRadio(void)
//{
//NRF_RADIO->EVENTS_DISABLED = 0U;
//NRF_RADIO->TASKS_DISABLE = 1U;
//NRF_RADIO->PACKETPTR = (uint32_t) &packet;
//}

static __inline void handleIncommingPacket(void)
{
/*	portBASE_TYPE xHigherPriorityTaskWoken;

	if (!CRTP_IS_NULL_PACKET(packet.crtp))  {//Don't follow the NULL packets
		  xHigherPriorityTaskWoken = pdFALSE;
		  xQueueSendFromISR( rxq, &packet, &xHigherPriorityTaskWoken);
		  if(xHigherPriorityTaskWoken)
		    vPortYieldFromISR();
	  }
*/
}

void RADIO_IRQHandler()
{
  static uint8_t last_pid=255;
  static uint32_t last_crc=0;

  if (NRF_RADIO->EVENTS_DISABLED) {
	  NRF_RADIO->EVENTS_DISABLED = 0UL;

	  if (!NRF_RADIO->CRCSTATUS) {  //Wrong CRC packet are dropped
      enableRx();
      return;
    }

    switch (rs){
    case doRx:
//      if (rxPacket.raw.pid != last_pid &&
//                 NRF_RADIO->RXCRC != last_crc)  // Good packet are used
//      {
        last_pid = rxPacket[pkq_head].raw.pid;
        last_crc = NRF_RADIO->RXCRC;

        rxPacket[pkq_head].rssi = NRF_RADIO->RSSISAMPLE;
        rxPacket[pkq_head].address = NRF_RADIO->RXMATCH;

        pkq_head = pkq_inc(pkq_head);


        enableRx();
        NRF_GPIO->OUTSET = 1<<19;
//      } else {  //Repeated packet are dropped but the acked data are re-sent
//        rerunRx = true;
//        enableRx();
//      }
      break;
    case doTx:
      //Nothing really done hear. Receiver is re-enabled by the main loop
      //ToDo: Double buffering!

      if(rerunRx) {
        enableRx();
        rerunRx = false;
      }

      NRF_GPIO->OUTCLR = 1<<19;
      break;
    }
  }
}

void nrfSetChannel(unsigned int channel)
{
	if (channel < 126) {
	NRF_RADIO->FREQUENCY = channel;
	}
}

void nrfSetDatarate(int datarate)
{
	switch (datarate) {
	case RADIO_RATE_250K:
		  NRF_RADIO->MODE = (RADIO_MODE_MODE_Nrf_250Kbit << RADIO_MODE_MODE_Pos);
		  break;
	case RADIO_RATE_1M:
		  NRF_RADIO->MODE = (RADIO_MODE_MODE_Nrf_1Mbit << RADIO_MODE_MODE_Pos);
		  break;
	case RADIO_RATE_2M:
		  NRF_RADIO->MODE = (RADIO_MODE_MODE_Nrf_2Mbit << RADIO_MODE_MODE_Pos);
		  break;
	}
}

/* Initialisation */
void nrfInit(void)
{
  if (isInit)
    return;

  // Enable Radio interrupts
  NVIC_EnableIRQ(RADIO_IRQn);
  // Radio config
  NRF_RADIO->TXPOWER = (RADIO_TXPOWER_TXPOWER_0dBm << RADIO_TXPOWER_TXPOWER_Pos);

  // Radio address config
  // Using logical address 0 so only BASE0 and PREFIX0 & 0xFF are used
  NRF_RADIO->PREFIX0 = 0xC4C3ffE7UL;  // Prefix byte of addresses 3 to 0
  //NRF_RADIO->PREFIX0 = 0xC4C3C233UL;  // Prefix byte of addresses 3 to 0
  NRF_RADIO->PREFIX1 = 0xC5C6C7C8UL;  // Prefix byte of addresses 7 to 4
  NRF_RADIO->BASE0   = 0xE7E7E7E7UL;  // Base address for prefix 0
  //NRF_RADIO->BASE0   = 0x33333333UL;  // Base address for prefix 0
  NRF_RADIO->BASE1   = 0xE7E7E7E7UL;  // Base address for prefix 1-7
  NRF_RADIO->TXADDRESS = 0x00UL;      // Set device address 0 to use when transmitting
  NRF_RADIO->RXADDRESSES = (1<<0) | (1<<1) | (1<<2);    // Enable device address 0 to use which receiving

  // Packet configuration
  NRF_RADIO->PCNF0 = (PACKET0_S1_SIZE << RADIO_PCNF0_S1LEN_Pos) |
                     (PACKET0_S0_SIZE << RADIO_PCNF0_S0LEN_Pos) |
                     (PACKET0_PAYLOAD_SIZE << RADIO_PCNF0_LFLEN_Pos); //lint !e845 "The right argument to operator '|' is certain to be 0"

  // Packet configuration
   NRF_RADIO->PCNF1 = (RADIO_PCNF1_WHITEEN_Disabled << RADIO_PCNF1_WHITEEN_Pos)    |
                      (RADIO_PCNF1_ENDIAN_Big << RADIO_PCNF1_ENDIAN_Pos)           |
                      (PACKET1_BASE_ADDRESS_LENGTH << RADIO_PCNF1_BALEN_Pos)       |
                      (PACKET1_STATIC_LENGTH << RADIO_PCNF1_STATLEN_Pos)           |
                      (PACKET1_PAYLOAD_SIZE << RADIO_PCNF1_MAXLEN_Pos); //lint !e845 "The right argument to operator '|' is certain to be 0"

  // CRC Config
  NRF_RADIO->CRCCNF = (RADIO_CRCCNF_LEN_Two << RADIO_CRCCNF_LEN_Pos); // Number of checksum bits
  NRF_RADIO->CRCINIT = 0xFFFFUL;      // Initial value
  NRF_RADIO->CRCPOLY = 0x11021UL;     // CRC poly: x^16+x^12^x^5+1

  // Set pointer to packet payload
  NRF_RADIO->PACKETPTR = (uint32_t) &rxBuffers[0].packet;

  // Enable interrupt for events
  NRF_RADIO->INTENSET = /*RADIO_INTENSET_END_Msk |*/ RADIO_INTENSET_DISABLED_Msk;

  NRF_RADIO->SHORTS = RADIO_SHORTS_READY_START_Msk;
  NRF_RADIO->SHORTS |= RADIO_SHORTS_ADDRESS_RSSISTART_Msk;
  NRF_RADIO->SHORTS |= RADIO_SHORTS_END_DISABLE_Msk;

  enableRx();

  isInit = true;
}

bool nrfTest(void)
{
  //TODO implement real tests!
  return isInit;
}

bool nrfIsPacketReceived()
{
  return (pkq_head != pkq_tail);
}

RadioPacket *nrfGetRxPacket() {
  RadioPacket *pk = NULL;

  if (nrfIsPacketReceived())
    pk = &rxPacket[pkq_tail];

  return pk;
}

void nrfReleaseRxPacket(RadioPacket *packet) {
  pkq_tail = pkq_inc(pkq_tail);
}

bool nrfIsPacketSent()
{
  return packetSent && !newTxPacket;
}

RadioPacket *nrfGetTxPacket()
{
  return &txPacket;
}

void nrfSendTxPacket()
{
  newTxPacket = true;
}

