#include <stdbool.h>

#include <nrf.h>

#include "pinout.h"

#include "uart.h"


#define Q_LENGTH 100

static char rxq[Q_LENGTH];
static int head = 0;
static int tail = 0;

static int dropped = 0;
static char dummy;

void UART0_IRQHandler()
{
  int nhead = head+1;
  
  NRF_UART0->EVENTS_RXDRDY = 0;
  
  // Check if the queue is not full
  if (nhead >= Q_LENGTH) nhead = 0;
  if (nhead == tail) {
    dummy = NRF_UART0->RXD; //Read anyway to avoid hw overflow
    dropped++;
    return;
  }
  
  // Push data in queue
  rxq[head++] = NRF_UART0->RXD;
  if (head >= Q_LENGTH) head = 0;
}

void uartInit()
{
  NRF_GPIO->PIN_CNF[8] &= ~GPIO_PIN_CNF_PULL_Msk;
  NRF_GPIO->PIN_CNF[8] |= (GPIO_PIN_CNF_PULL_Pulldown<<GPIO_PIN_CNF_PULL_Pos);

  NRF_GPIO->DIRSET = 1<<UART_TX_PIN;
  NRF_GPIO->OUTSET = 1<<UART_TX_PIN;
  NRF_UART0->PSELTXD = UART_TX_PIN;
  
  NRF_GPIO->DIRCLR = 1<<UART_RX_PIN;
  NRF_UART0->PSELRXD = UART_RX_PIN;
  
  NRF_UART0->BAUDRATE = UART_BAUDRATE_BAUDRATE_Baud460800;
  
  NRF_UART0->ENABLE = UART_ENABLE_ENABLE_Enabled;

  NRF_UART0->TASKS_STARTTX = 1;
  
  // Enable interrupt on receive
  NVIC_EnableIRQ(UART0_IRQn);
  NRF_UART0->INTENSET = UART_INTENSET_RXDRDY_Msk;
  
  NRF_UART0->TASKS_STARTRX = 1;
}

void uartPuts(char* string)
{
  while(*string)
  {
    uartPutc(*string++);
  }
}

void uartSend(char* data, int len)
{
  while(len--)
  {
    uartPutc(*data++);
  }
}

void uartPutc(char c)
{
  NRF_UART0->TXD = c;
  while(!NRF_UART0->EVENTS_TXDRDY);
  NRF_UART0->EVENTS_TXDRDY=0;
}

bool uartIsDataReceived()
{
  return head!=tail;
}

char uartGetc()
{
  char c=0;
  
  if (head!=tail) {
    c = rxq[tail++];
    if (tail >= Q_LENGTH) tail = 0;
  }
  
  return c;
}
