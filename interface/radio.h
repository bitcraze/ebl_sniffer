/*
 *    ||          ____  _ __                           
 * +------+      / __ )(_) /_______________ _____  ___ 
 * | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Crazyflie control firmware
 *
 * Copyright (C) 2011-2012 Bitcraze AB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, in version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * nrf24l01.h: nRF24L01(-p) PRX mode low level driver
 */

#ifndef __NRF24L01_H__
#define __NRF24L01_H__

#include <stdint.h>
#include <stdbool.h>

// Init and test of the connection to the chip
void nrfInit(void);
bool nrfTest(void);

// Interrupt routine
void nrfIsr();

/*** Defines ***/
#define RADIO_RATE_250K 0
#define RADIO_RATE_1M 1
#define RADIO_RATE_2M 2

//Union used to efficiently handle the packets (Private type)
typedef struct
{
  //CRTPPacket crtp;
  struct {
    uint8_t size;
    union {
      uint8_t s1;
      struct {
        uint8_t noack :1;
        uint8_t pid :2;
      };
    };
    uint8_t data[32];
  } __attribute__((packed)) raw;
  uint8_t rssi;
  uint8_t address;
} RadioPacket;

struct radioBuffer {
  RadioPacket packet;
  bool busy;
};

void nrfSetChannel(unsigned int channel);
void nrfSetDatarate(int datarate);
unsigned char nrfGetStatus();

bool nrfIsPacketReceived();
RadioPacket *nrfGetRxPacket();
void nrfReleaseRxPacket(RadioPacket *packet);

bool nrfIsPacketSent();
RadioPacket *nrfGetTxPacket();
void nrfSendTxPacket();


#endif
