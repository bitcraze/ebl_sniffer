#ifndef __PINOUT_H__
#define __PINOUT_H__

#define UART_RX_PIN 25
#define UART_TX_PIN 24

#define PM_VCCEN_PIN 19
#define PM_SWITCH_PIN 16

#define PM_VBAT_PIN 1
#define PM_VBAT_SINK_PIN 2
#define PM_CHG_PIN 3
#define PM_PGOOD 4
#define PM_EN2 5
#define PM_EN1 6
#define PM_CHG_EN 7

#define OW_WKUP_PIN 8

#define LED_PIN 18

#define RADIO_PAEN_PIN 0

#define STM_BOOT0_PIN 1
#define STM_NRST_PIN 2

#endif //__PINOUT_H__

