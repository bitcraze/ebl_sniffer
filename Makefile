CROSS_COMPILE=arm-none-eabi-

CC=$(CROSS_COMPILE)gcc
AS=$(CROSS_COMPILE)as
LD=$(CROSS_COMPILE)gcc


INCLUDES= -I Include -I Include/gcc -Iinterface

PROCESSOR = -mcpu=cortex-m0 -mthumb
NRF= -DNRF51

CFLAGS=$(PROCESSOR) $(NRF) $(INCLUDES) -g3 -Os
# --specs=nano.specs
ASFLAGS=$(PROCESSOR)
LDFLAGS=$(PROCESSOR) -T gcc_nrf51_blank_xxaa.ld --specs=nano.specs


OBJS=src/main.o gcc_startup_nrf51.o system_nrf51.o src/uart.o src/radio.o
PROGRAM=cf2_nrf

all: $(PROGRAM).elf $(PROGRAM).bin
	arm-none-eabi-size $(PROGRAM).elf

$(PROGRAM).bin: $(PROGRAM).elf
	arm-none-eabi-objcopy $^ -O binary $@

$(PROGRAM).elf: $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^

clean:
	rm -f $(PROGRAM).bin $(PROGRAM).elf $(OBJS)

gdbserver:
	JLinkGDBServer -device NRF51822 -if swd

flash: $(PROGRAM).bin
	./generate_flash_script.rb $^
	-JLinkExe -if swd -speed 4000 .flash.jlink
	rm .flash.jlink

reset:
	-JLinkExe reset.jlink
